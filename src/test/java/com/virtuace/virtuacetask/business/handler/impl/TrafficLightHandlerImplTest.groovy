package com.virtuace.virtuacetask.business.handler.impl

import com.virtuace.virtuacetask.business.handler.TrafficLightHandler
import com.virtuace.virtuacetask.business.properties.LightTimingsProperties
import com.virtuace.virtuacetask.entities.business.enums.Light
import spock.lang.Specification
import spock.lang.Subject

class TrafficLightHandlerImplTest extends Specification {

    @Subject
    TrafficLightHandler textParseHandler = new TrafficLightHandlerImpl(getProperties())

    def "Test replace text by regex"() {
        when:
        def lightResult
        try {
            lightResult = textParseHandler.getTrafficLightByPastHourMinute(timeMin as Double)
        } catch (Exception e) {
            //TODO i have not enough time but we can handle exceptions on `Handler` layer (wrap into something like `Response`)
            lightResult = "Some Exception"
        }
        then:
        lightResult == lightResultTest
        where:
        timeMin      | lightResultTest
        1            | Light.GREEN
        1.00001      | Light.GREEN
        1.5          | Light.GREEN
        1.4354336346 | Light.GREEN
        3            | Light.YELLOW
        3.99         | Light.YELLOW
        3.999        | Light.YELLOW
        3.999999999  | Light.YELLOW
        4            | Light.RED
        4.0001       | Light.RED
        4.999999     | Light.RED
        1222         | "Some Exception"
        1222.25      | "Some Exception"
        1222.3333    | "Some Exception"
        -50          | "Some Exception"
    }

    LightTimingsProperties getProperties() {
        Map<Light, Integer> lightTimings = new TreeMap<>()
        lightTimings.put(Light.GREEN, 3);
        lightTimings.put(Light.YELLOW, 1);
        lightTimings.put(Light.RED, 1);
        return new LightTimingsProperties(lightTimings)
    }
}
