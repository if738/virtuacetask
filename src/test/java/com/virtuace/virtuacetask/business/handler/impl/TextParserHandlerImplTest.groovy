package com.virtuace.virtuacetask.business.handler.impl


import com.virtuace.virtuacetask.business.handler.TextParseHandler
import spock.lang.Specification
import spock.lang.Subject

class TextParserHandlerImplTest extends Specification {

    @Subject
    TextParseHandler textParseHandler = new TextParseHandlerImpl()

    def "Test replace text by regex"() {
        when:
        def textResult = textParseHandler.parseByRegex(text)
        then:
        textResult == textResultTest

        where:
        text                                                        || textResultTest
        null                                                        || ""
        ""                                                          || ""
        " "                                                         || ""
        "+123456789123456"                                          || ""
        "愛はあらゆる門を開く"                                        || ""
        "+لياليليالي"                                                   || ""
        "123123123"                                                 || ""
        "🖤"                                                         || ""
        "555 Straight Stave Ave, San Francisco, CA 94104"           || "555 Straight Stave Avenue, San Francisco, CA 94104"
        "333 Straight Stave Ave., San Francisco, CA 94104"          || "333 Straight Stave Avenue, San Francisco, CA 94104"
        "222 Ave Maria Stairway St, San Francisco, CA 94104"        || "222 Ave Maria Stairway Street, San Francisco, CA 94104"
        "444 Ave Maria Stairway St., San Francisco, CA 94104"       || "444 Ave Maria Stairway Street, San Francisco, CA 94104"
        "9032 Flave Steep Str, San Francisco, CA 94104"             || "9032 Flave Steep Street, San Francisco, CA 94104"
        "9031 Flave Steep Str., San Francisco, CA 94104"            || "9031 Flave Steep Street, San Francisco, CA 94104"
    }

}
