package com.virtuace.virtuacetask.business.properties;

import com.virtuace.virtuacetask.business.utils.ArrayUtils;
import com.virtuace.virtuacetask.entities.business.enums.Light;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by Alexey Podgorny on 20.12.2020.
 */

@Data
@Slf4j
@Component
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(value = "virtuace.business.light",
        ignoreUnknownFields = false,
        ignoreInvalidFields = false)
public class LightTimingsProperties {

    private Map<Light, Integer> values = new TreeMap<>(Comparator.comparing(Light::getSequence));

    @PostConstruct
    public void verifyData() {
        Integer timingsSum = ArrayUtils.sumIntValues(this.values.values());
        if (TimeUnit.HOURS.toMinutes(1) % timingsSum != 0) {
            log.error("Critical error, sum of timings is not multiple to 60, timings sum={}", timingsSum);
            throw new ArithmeticException("LightTimingsProperties class. sum of timings is not multiple to 60");
        }
    }

}
