package com.virtuace.virtuacetask.business.handler.impl;

import com.virtuace.virtuacetask.business.handler.RecursionHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */

@Slf4j
@Component
@RequiredArgsConstructor
public class RecursionHandlerImpl implements RecursionHandler {

    @Override
    public List<Number> getAllNumbers(long targetNumber) {
        log.trace("getAllNumbers. method started, targetNumber={}", targetNumber);
        recursionPrinter(targetNumber);
        log.debug("getAllNumbers. method finished, targetNumber={}", targetNumber);
        return LongStream.range(1, targetNumber + 1)
                .boxed()
                .collect(Collectors.toList());
    }

    private void recursionPrinter(long current) {
        if (current == 0) return;
        recursionPrinter(current - 1);
        log.info("Recursion, number={}", current);
    }

}
