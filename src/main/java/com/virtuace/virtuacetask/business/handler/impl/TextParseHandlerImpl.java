package com.virtuace.virtuacetask.business.handler.impl;

import com.virtuace.virtuacetask.business.handler.TextParseHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */

@Slf4j
@Component
@RequiredArgsConstructor
public class TextParseHandlerImpl implements TextParseHandler {

    @Override
    public String parseByRegex(String rawText) {
        log.trace("parseByRegex. method started, rawText={}", rawText);
        if (!StringUtils.hasText(rawText)) {
            log.warn("parseByRegex. text is empty, rawText={}", rawText);
            return "";
        }
        String replacedToAvenue = replaceFirstByKeysByRegex(rawText,
                Arrays.asList("Ave", "Ave."),
                "Avenue");
        String replacedToAvenueAndSt = replaceFirstByKeysByRegex(replacedToAvenue,
                Arrays.asList("St", "St.", "Str", "Str."),
                "Street");
        if (rawText.equals(replacedToAvenueAndSt)) {
            log.trace("replaceFirstByKeysByRegex. Nothing to replace, rawText={} replacedToAvenue={} replacedToAvenueAndSt={}",
                    rawText, replacedToAvenue, replacedToAvenueAndSt);
            return "";
        }
        return replacedToAvenueAndSt;
    }

    private String replaceFirstByKeysByRegex(String rawText, List<String> keys, String value) {
        String text;
        try {
            text = "";
            for (String k : keys) {
                text = rawText.replaceAll("\\s" + k + ",", " " + value + ",");
                if (!rawText.equals(text)) return text;
            }
        } catch (Exception e) {
            log.error("replaceFirstByKeysByRegex. Api error, rawText={} keys={} value={} error={}", rawText, keys, value, e.getMessage());
            return "";
        }
        return text;
    }

    @Override
    public String parseByAnotherWay(String rawText) {
        log.trace("parseByAnotherWay. method started, rawText={}", rawText);
        if (!StringUtils.hasText(rawText)) {
            log.warn("parseByAnotherWay. text is empty, rawText={}", rawText);
            return "";
        }
        String replacedToAvenue = replaceFirstByKeysByAnotherWay(rawText, Arrays.asList("Ave", "Ave."),
                "Avenue");
        return replaceFirstByKeysByAnotherWay(replacedToAvenue,
                Arrays.asList("St", "St.", "Str", "Str."),
                "Street");
    }

    private String replaceFirstByKeysByAnotherWay(String rawText, List<String> keys, String value) {
        try {
            int firstCharPosition = 0;
            String foundKey = "";
            for (String k : keys) {
                int i = rawText.indexOf(k + ",");
                if (i != -1) {
                    foundKey = k;
                    firstCharPosition = i;
                    return rawText.substring(0, firstCharPosition) + value + rawText.substring(firstCharPosition + foundKey.length());
                }
            }
        } catch (Exception e) {
            log.error("replaceFirstByKeysByAnotherWay. Api error, rawText={} keys={} value={} error={}", rawText, keys, value, e.getMessage());
            return "";
        }
        return "";
    }

}
