package com.virtuace.virtuacetask.business.handler;

import java.util.List;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */
public interface RecursionHandler {

    List<Number> getAllNumbers(long targetNumber);

}
