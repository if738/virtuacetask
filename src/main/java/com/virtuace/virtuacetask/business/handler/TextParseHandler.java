package com.virtuace.virtuacetask.business.handler;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */
public interface TextParseHandler {

    String parseByRegex(String rawText);

    String parseByAnotherWay(String rawText);

}
