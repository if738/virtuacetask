package com.virtuace.virtuacetask.business.handler.impl;

import com.virtuace.virtuacetask.business.handler.TrafficLightHandler;
import com.virtuace.virtuacetask.business.properties.LightTimingsProperties;
import com.virtuace.virtuacetask.business.utils.ArrayUtils;
import com.virtuace.virtuacetask.business.utils.TimeUtils;
import com.virtuace.virtuacetask.entities.business.data.TimeInterval;
import com.virtuace.virtuacetask.entities.business.enums.Light;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.time.LocalTime;
import java.util.*;

/**
 * Created by Alexey Podgorny on 20.12.2020.
 */

@Slf4j
@Component
@RequiredArgsConstructor
public class TrafficLightHandlerImpl implements TrafficLightHandler {

    private final LightTimingsProperties properties;

    @Override
    public Light getTrafficLightByPastHourMinute(Double timeMin) {
        log.trace("getTrafficLightByPastHourMinute. method started, timeMin={}", timeMin);
        if (ObjectUtils.isEmpty(timeMin)) {
            log.warn("parseByRegex. text is empty, timeMin={}", timeMin);
            return null;
        }
        Map<Light, List<TimeInterval<LocalTime>>> intervals = getIntervals(properties.getValues());
        LocalTime minWithSec = TimeUtils.parseFromMin(timeMin);
        int minute = minWithSec.getMinute();
        int second = minWithSec.getSecond();
        return getLightByMinute(intervals, minute, second);
    }

    private Light getLightByMinute(Map<Light, List<TimeInterval<LocalTime>>> intervals, Integer minute, Integer second) {
        Light light;
        try {
            light = intervals.entrySet().stream()
                    .filter(entry -> new TimeInterval<>(LocalTime.of(0, minute, second), LocalTime.of(0, minute, second))
                            .isAnyIntersected(entry.getValue()))
                    .findFirst().stream()
                    .map(Map.Entry::getKey)
                    .findFirst()
                    .orElseThrow(NoSuchElementException::new);
        } catch (Exception e) {
            log.error("getLightByMinute. Api error, Can't find Light by minute, intervals={} minute={} error={}", intervals, minute, e.getMessage());
            throw e;
        }
        return light;
    }

    private Map<Light, List<TimeInterval<LocalTime>>> getIntervals(Map<Light, Integer> values) {
        Map<Light, List<TimeInterval<LocalTime>>> lightIntervals = new HashMap<>();
        Set<Light> lights = values.keySet();
        Integer sumTimings = ArrayUtils.sumIntValues(values.values());
        Integer currentIterationTiming = 0;
        for (Light light : lights) {
            Integer timing = values.get(light);
            List<TimeInterval<LocalTime>> intervals = new ArrayList<>();
            LocalTime startTime;
            LocalTime endTime;
            for (int j = currentIterationTiming; j < 60; j = j + sumTimings) {
                startTime = LocalTime.of(0, j);
                if (j + timing <= 59) {
                    endTime = LocalTime.of(0, j + timing);
                } else endTime = LocalTime.of(1, 0);
                intervals.add(new TimeInterval<>(startTime, endTime));
            }
            currentIterationTiming = currentIterationTiming + timing;
            lightIntervals.put(light, intervals);
        }
        return lightIntervals;
    }

}
