package com.virtuace.virtuacetask.business.handler;

import com.virtuace.virtuacetask.entities.business.enums.Light;

/**
 * Created by Alexey Podgorny on 20.12.2020.
 */
public interface TrafficLightHandler {

    Light getTrafficLightByPastHourMinute(Double timeMin);

}
