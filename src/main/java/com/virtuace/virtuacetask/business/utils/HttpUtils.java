package com.virtuace.virtuacetask.business.utils;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */
public interface HttpUtils {

    static String toString(HttpServletRequest request) {
        var str = request.getRequestURL();
        var queryString = request.getQueryString();
        if (queryString != null) {
            str.append("?").append(queryString);
        }
        str.append(" Headers=").append(getHeaders(request).toString());
        return str.toString();
    }


    static MultiValueMap<String, String> getHeaders(HttpServletRequest request) {
        var map = new LinkedMultiValueMap<String, String>();
        var headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            var key = headerNames.nextElement();
            var value = request.getHeader(key);
            map.add(key, value);
        }
        return map;
    }

}
