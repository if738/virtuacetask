package com.virtuace.virtuacetask.business.utils;

import org.springframework.util.ObjectUtils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalTime;

/**
 * Created by Alexey Podgorny on 20.12.2020.
 */
public interface TimeUtils {

    static LocalTime parseFromMin(Double time) {
        if (ObjectUtils.isEmpty(time)) return LocalTime.MIN;
        DecimalFormat f = new DecimalFormat("##.000");
        f.setRoundingMode(RoundingMode.FLOOR);
        int min = Integer.parseInt(f.format(time).replaceAll("\\..*", ""));
        Double sec = Double.parseDouble(f.format(time).replaceAll(".*\\.", "")) / 1000 * 60;
        return LocalTime.of(0, min, sec.intValue());
    }

}
