package com.virtuace.virtuacetask.business.utils;

import java.util.Collection;

/**
 * Created by Alexey Podgorny on 20.12.2020.
 */
public interface ArrayUtils {

    static Integer sumIntValues(Collection<Integer> numbers) {
        return numbers.stream()
                .reduce(0, Integer::sum);
    }

}
