package com.virtuace.virtuacetask.business.validation;

import com.virtuace.virtuacetask.entities.web.Response;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */
public interface ValidateTrafficLight {

    Response<Void> validate(Double timeMin);

}
