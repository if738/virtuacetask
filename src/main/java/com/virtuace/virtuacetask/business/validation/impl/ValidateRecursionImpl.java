package com.virtuace.virtuacetask.business.validation.impl;

import com.virtuace.virtuacetask.business.validation.ValidateRecursion;
import com.virtuace.virtuacetask.entities.web.Error;
import com.virtuace.virtuacetask.entities.web.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.virtuace.virtuacetask.entities.web.enums.BasicErrorSubType.INVALID_NUMBER;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ValidateRecursionImpl implements ValidateRecursion {

    @Override
    public Response<Void> validate(Long number) {
        String subError = INVALID_NUMBER.toString();
        String error = INVALID_NUMBER.getErrorType();
        if (number < 0 || number > 8000) return Response.of(Error.of(error, subError,
                "Number must positive and less then 8000, it may produce StackOverflowError"));
        return Response.empty();
    }

}
