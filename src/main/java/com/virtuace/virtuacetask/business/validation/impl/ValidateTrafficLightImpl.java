package com.virtuace.virtuacetask.business.validation.impl;

import com.virtuace.virtuacetask.business.validation.ValidateTrafficLight;
import com.virtuace.virtuacetask.entities.web.Error;
import com.virtuace.virtuacetask.entities.web.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.virtuace.virtuacetask.entities.web.enums.BasicErrorSubType.INVALID_TIME;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ValidateTrafficLightImpl implements ValidateTrafficLight {

    @Override
    public Response<Void> validate(Double timeMin) {
        String subError = INVALID_TIME.toString();
        String error = INVALID_TIME.getErrorType();
        if (timeMin > 59 || timeMin < 0) return Response.of(Error.of(error, subError,
                "Input time should between 0 and 59"));
        return Response.empty();
    }

}
