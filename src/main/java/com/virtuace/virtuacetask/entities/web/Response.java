package com.virtuace.virtuacetask.entities.web;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.util.ObjectUtils;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static com.fasterxml.jackson.annotation.JsonCreator.Mode.DISABLED;
import static com.fasterxml.jackson.annotation.JsonCreator.Mode.PROPERTIES;

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Response<T> {
    private static final Response<?> EMPTY = new Response<>(true, null, null);

    private final boolean success;
    private final Error error;
    private final T data;
    private String requestId;

    @JsonCreator(mode = PROPERTIES)
    public static <T> Response<T> of(@JsonProperty("data") T data) {
        return new Response<>(true, null, data);
    }

    @JsonCreator(mode = DISABLED)
    public static <T> Response<T> of(Error error) {
        return new Response<>(false, error, null);
    }

    public static <T> Response<T> empty() {
        @SuppressWarnings("unchecked")
        Response<T> t = (Response<T>) EMPTY;
        return t;
    }

    @JsonIgnore
    public boolean isNotSuccess() {
        return !this.isSuccess();
    }

    @JsonIgnore
    public boolean isSuccessAndNotEmpty() {
        return this.isSuccess() && !isEmpty();
    }

    @JsonIgnore
    public boolean isEmpty() {
        return ObjectUtils.isEmpty(this.data);
    }

    @JsonIgnore
    public boolean notSuccess() {
        return !isSuccess();
    }

    public Optional<T> dataOptional() {
        return Optional.ofNullable(data);
    }

    public <K> Response<K> map(Function<T, K> f) {
        return notSuccess() ? Response.of(error) : Response.of(f.apply(data));
    }

    public Response<T> validate(Function<T, Response<?>> f) {
        if (notSuccess()) return this;
        Response<?> response = f.apply(data);
        return response.notSuccess() ? Response.of(response.getError()) : this;
    }

    public <K> Response<K> flatMap(Function<T, Response<K>> f) {
        return notSuccess() ? Response.of(error) : f.apply(data);
    }

    public T orElseGet(T defaultData) {
        return notSuccess() ? defaultData : data;
    }

    public Response<T> orElse(Response<T> alternative) {
        return notSuccess() ? alternative : this;
    }

    public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier) throws X {
        if (isSuccess()) {
            return data;
        }
        throw exceptionSupplier.get();
    }

    public <K> Response<K> andThen(Supplier<Response<K>> next) {
        return notSuccess() ? Response.of(error) : next.get();
    }

    public Response<T> peek(Consumer<T> consumer) {
        if (notSuccess()) return this;
        consumer.accept(data);
        return this;
    }

    public void forEach(Consumer<T> f) {
        if (isSuccess()) {
            f.accept(data);
        }
    }

}
