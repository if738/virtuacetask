package com.virtuace.virtuacetask.entities.business.data;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Created by Alexey Podgorny on 27.03.2019.
 */
@Data
@Slf4j
public class TimeInterval<T extends Comparable<? super T>> {

    T startTime;
    T endTime;

    public TimeInterval(T startTime, T endTime) {
        if (startTime.compareTo(endTime) > 0) {
            log.error("startTime must be less then endTime, startTime={} endTime={}", startTime, endTime);
            throw new ArithmeticException("TimeInterval class. startTime must be less then endTime");
        }
        this.startTime = startTime;
        this.endTime = endTime;
    }


    public boolean isAnyIntersected(TimeInterval<T> timeInterval) {
        return endTime.compareTo(timeInterval.startTime) >= 0 && startTime.compareTo(timeInterval.endTime) < 0;
    }

    public boolean isAnyIntersected(List<TimeInterval<T>> timeIntervals) {
        return timeIntervals.stream().anyMatch(this::isAnyIntersected);
    }


}
