package com.virtuace.virtuacetask.entities.business.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by Alexey Podgorny on 20.12.2020.
 */
@Getter
@RequiredArgsConstructor
public enum Light {

    GREEN("green", 1),
    YELLOW("yellow", 2),
    RED("red", 3);

    private final String name;
    private final int sequence;

}
