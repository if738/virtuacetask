package com.virtuace.virtuacetask.web.controller;

import com.virtuace.virtuacetask.business.handler.TextParseHandler;
import com.virtuace.virtuacetask.entities.web.Response;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */

@Slf4j
@RestController
@RequestMapping("/api/text")
@AllArgsConstructor
public class TextController {

    private final TextParseHandler textParseHandler;

    @GetMapping("/regex")
    public Response<String> getRightTextByRegex(@RequestParam String rawText) {
        log.trace("getRightTextByRegex. init, rawText={}", rawText);
        return Response.of(rawText)
                .map(textParseHandler::parseByAnotherWay);
    }

    @GetMapping("/anotherWay")
    public Response<String> getRightTextByAnotherWay(@RequestParam String rawText) {
        log.trace("getRightTextByAnotherWay. init, rawText={}", rawText);
        return Response.of(rawText)
                .map(textParseHandler::parseByAnotherWay);
    }

}
