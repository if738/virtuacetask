package com.virtuace.virtuacetask.web.controller;

import com.virtuace.virtuacetask.business.handler.RecursionHandler;
import com.virtuace.virtuacetask.business.validation.ValidateRecursion;
import com.virtuace.virtuacetask.entities.web.Response;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */
@Slf4j
@RestController
@RequestMapping("/api/recursion")
@AllArgsConstructor
public class RecursionController {

    private final RecursionHandler recursionHandler;
    private final ValidateRecursion validateRecursion;

    @GetMapping
    public Response<List<Number>> getRightTextByRegex(@RequestParam Long number) {
        log.trace("getRightTextByRegex. init, number={}", number);
        return Response.of(number)
                .validate(validateRecursion::validate)
                .map(recursionHandler::getAllNumbers);
    }


}
