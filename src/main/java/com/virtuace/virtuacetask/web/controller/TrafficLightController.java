package com.virtuace.virtuacetask.web.controller;

import com.virtuace.virtuacetask.business.handler.TrafficLightHandler;
import com.virtuace.virtuacetask.business.validation.ValidateTrafficLight;
import com.virtuace.virtuacetask.entities.business.enums.Light;
import com.virtuace.virtuacetask.entities.web.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Alexey Podgorny on 21.12.2020.
 */

@Slf4j
@RestController
@RequestMapping("/api/traffic/light")
@RequiredArgsConstructor
public class TrafficLightController {

    private final TrafficLightHandler trafficLightHandler;
    private final ValidateTrafficLight validateTrafficLight;

    @GetMapping
    public Response<Light> getLight(@RequestParam Double timeMin) {
        log.trace("getLight. init, timeMin={}", timeMin);
        return Response.of(timeMin)
                .validate(validateTrafficLight::validate)
                .map(trafficLightHandler::getTrafficLightByPastHourMinute);
    }

}
