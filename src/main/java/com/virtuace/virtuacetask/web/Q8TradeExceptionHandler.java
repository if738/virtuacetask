package com.virtuace.virtuacetask.web;

import com.virtuace.virtuacetask.business.utils.HttpUtils;
import com.virtuace.virtuacetask.entities.web.Error;
import com.virtuace.virtuacetask.entities.web.Response;
import com.virtuace.virtuacetask.entities.web.enums.BasicErrorType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestControllerAdvice
public class Q8TradeExceptionHandler {

    @ExceptionHandler(Throwable.class)
    public Response<Void> generalHandler(Exception ex, HttpServletRequest request) {
        log.error(getLoggerMsg(ex.getMessage(), request), ex);
        return Response.of(Error.of(BasicErrorType.UNEXPECTED_ERROR, "Throwable", ex.getMessage()));
    }

    private String getLoggerMsg(String message, HttpServletRequest request) {
        return "Failed to handle request, error=" + message + " request=" + HttpUtils.toString(request);
    }
}
