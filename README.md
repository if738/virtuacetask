Java 11
Hi, i'm Lesha :)

Package structure and approach I inherited from my previous work.
I would like to provide several notes for better understanding:
1. I started from third task (about lights), it seemed to me the biggest. I provided possibility to manage timings from application.properties file. The main idea was to provide more flaxible solution, because sometimes customers want to change something little like timings or maybe add forth color... So I spend most part of time on this task, and two other could have been better.
2. We didn't use HTTP codes, always 200 with Response<?>;
3. log.type("methodName. some details, p1={} p2={} p3={}", my log convention should be the same everywhere.
4. Every Handler,Service,Validator etc.. should have interface.
5. Controllers may contain only upper logic (handlers,validators,services) and maybe some http logic;
6. Q8TradeExceptionHandler and Response<?> I stole from my previuos project :)
7. http request examples:
`curl --location --request GET 'http://localhost:8080/api/traffic/light?timeMin=5'`
`curl --location --request GET 'http://localhost:8080/api/text?rawText=555%20Straight%20Stave%20Ave,%20San%20Francisco,%20CA%2094104'`
`curl --location --request GET 'http://localhost:8080/api/recursion?number=22'`
